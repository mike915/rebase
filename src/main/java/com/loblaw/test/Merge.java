package com.loblaw.test;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Merge {
    public int num;
}
